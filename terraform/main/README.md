# DNS

Defines the domain, hosted zone, and general resource record sets for `pink-ribbon-belgium`.

## AWS domain registration and DNS records

We registered a domain in the AWS console for pink-ribbon-belgium.org, as this can not be done from terraform.
This process by AWS, creates its own hosted zone, different from the one we created.
We can use that hosted zone instead, but that leaves us with less control over the nameserver records.
Therefor, we change the registered domain so it points to the hosted zone we created via terraform.

Registered domain pink-ribbon-belgium.org in AWS Console.
This domain had its own hosted zone, configured by the AWS Route53 Registrar.
It has these nameservers:
- ns-1641.awsdns-13.co.uk
- ns-880.awsdns-46.net
- ns-225.awsdns-28.com
- ns-1091.awsdns-08.org

We don't need them, because this hosted zone will not be used by us.
Instead, we go to the hosted zone we created via terraform and copy those nameservers.
These are the ones we will manually add to our bought registered domain.

It can not be done with terraform yet, as shown in this github issue:
https://github.com/terraform-providers/terraform-provider-aws/issues/88