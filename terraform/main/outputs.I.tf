output "I-hosted_zone-pink_ribbon_belgium_org" {
  value = {
    id   = aws_route53_zone.pink-ribbon-belgium-org.id
    name = aws_route53_zone.pink-ribbon-belgium-org.name
  }
}

output "I-hosted_zones" {
  value = [
    {
      id   = aws_route53_zone.pink-ribbon-belgium-org.id
      name = aws_route53_zone.pink-ribbon-belgium-org.name
    }
  ]
}


