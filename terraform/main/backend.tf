terraform {
  backend "s3" {
    bucket         = "tfstate.pink-ribbon-belgium.org"
    key            = "dns.tfstate"
    region         = "eu-west-1"
    profile        = "pink-ribbon-belgium-dev"
    encrypt        = true
    dynamodb_table = "tfstate-lock.pink-ribbon-belgium.org"
  }
}
data "terraform_remote_state" "infrastructure_role" {
  backend = "s3"
  config = {
    bucket  = "tfstate.pink-ribbon-belgium.org"
    key     = "dns-infrastructure_role.tfstate"
    region  = local.region
    profile = local.profile
  }
}