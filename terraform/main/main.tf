# Reserve 4 dns servers for use in NS resource record sets
resource "aws_route53_delegation_set" "main" {
  reference_name = "pink-ribbon-belgium"
}

resource "aws_route53_zone" "pink-ribbon-belgium-org" {
  name              = "pink-ribbon-belgium.org"
  delegation_set_id = aws_route53_delegation_set.main.id
}

resource "aws_route53_record" "www-pink-ribbon-belgium-org" {
  name    = "www"
  zone_id = aws_route53_zone.pink-ribbon-belgium-org.zone_id
  type    = "CNAME"
  ttl     = local.ttl
  weighted_routing_policy {
    weight = local.weight
  }
  set_identifier = "rozemars-be"
  records        = ["derozemars.be"]
}

# For dev, so they can test locally, but still as close to production as possible (communication over TLS)
resource "aws_route53_record" "www-pink-ribbon-belgium-org-localhost-ipv4" {
  name    = "localhost.dev.pink-ribbon-belgium.org"
  zone_id = aws_route53_zone.pink-ribbon-belgium-org.zone_id
  type    = "A"
  ttl     = local.ttl
  records = ["127.0.0.1"]
}

# For dev, so they can test locally, but still as close to production as possible (communication over TLS)
resource "aws_route53_record" "www-pink-ribbon-belgium-org-localhost-ipv6" {
  name    = "localhost.dev.pink-ribbon-belgium.org"
  zone_id = aws_route53_zone.pink-ribbon-belgium-org.zone_id
  type    = "AAAA"
  ttl     = local.ttl
  records = ["::1"]
}
