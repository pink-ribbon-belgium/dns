provider "external" { version = "~> 1.2" }

locals {
  profile = "pink-ribbon-belgium-dev"
  region  = "eu-west-1"
  ttl     = "60" # Seconds
  weight  = 10   # Weighted routing
}
