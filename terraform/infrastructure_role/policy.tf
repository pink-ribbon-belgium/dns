data "aws_iam_policy_document" "role-policy" {
  statement {
    effect = "Allow"
    actions = [
      "route53:CreateReusableDelegationSet",
      "route53:DeleteReusableDelegationSet",
      "route53:ListReusableDelegationSets",
      "route53:GetReusableDelegationSet",
      "route53:CreateHostedZone",
      "route53:DeleteHostedZone",
      "route53:ListHostedZones",
      "route53:GetHostedZone",
      "route53:GetChange",
      "route53:ListTagsForResource",
      "route53:ChangeTagsForResource",
      "route53:ChangeResourceRecordSets",
      "route53:ListResourceRecordSets"
    ]
    resources = ["*"]
  }
  statement {
    # devsecops users can manage devsecops policies
    effect = "Allow"
    actions = [
      "route53:CreateReusableDelegationSet",
      "route53:DeleteReusableDelegationSet",
      "route53:ListReusableDelegationSets",
      "route53:GetReusableDelegationSet",
      "route53:CreateHostedZone",
      "route53:DeleteHostedZone",
      "route53:ListHostedZones",
      "route53:GetHostedZone",
      "route53:GetChange",
      "route53:ListTagsForResource",
      "route53:ChangeTagsForResource",
      "route53:ChangeResourceRecordSets",
      "route53:ListResourceRecordSets",
      "route53:ListResourceRecordSets"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "role-policy" {
  name        = local.infrastructure-prefix
  policy      = data.aws_iam_policy_document.role-policy.json
  path        = "/devsecops/"
  description = ""
}
