provider "aws" {
  region  = local.region
  profile = local.profile
  version = "~> 2.43"
}

