locals {
  region                = "eu-west-1"
  profile               = "pink-ribbon-belgium-dev"
  repo                  = "dns"
  infrastructure-prefix = "${local.repo}-infrastructure"
}

data "aws_caller_identity" "current" {}
