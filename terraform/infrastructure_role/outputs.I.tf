output "I-infrastructure_role" {
  value = {
    path = aws_iam_role.dns-infrastructure.path
    name = aws_iam_role.dns-infrastructure.name
    arn  = aws_iam_role.dns-infrastructure.arn
  }
}

