terraform {
  backend "s3" {
    bucket         = "tfstate.pink-ribbon-belgium.org"
    key            = "dns-infrastructure_role.tfstate"
    region         = "eu-west-1"
    profile        = "pink-ribbon-belgium-dev"
    encrypt        = true
    dynamodb_table = "tfstate-lock.pink-ribbon-belgium.org"
  }
}

